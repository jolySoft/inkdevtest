﻿using System;
using System.Linq;
using FakeItEasy;
using InkDevTest.Service;
using InkDevTest.Service.BookingService;
using InkDevTest.Service.Dto;
using InkDevTest.Service.Mappings;
using NUnit.Framework;

namespace InkDevTest.units
{
    [TestFixture]
    public class AvailabilityServiceTests
    {
        private AvailabilityService _availabilityService;
        private BookingServiceProxy _bookingServiceProxy;

        [SetUp]
        public void Setup()
        {
            _bookingServiceProxy = A.Fake<BookingServiceProxy>();
            _availabilityService = new AvailabilityService(_bookingServiceProxy);

            //In not using a fake for the mappings this breaks true unit testing
            //because we're not isolating the AvailablityService but it's only a test.
            //Never do this in realLife©
            AutoMapping.CreateMaps();
        }

        [Test, ExpectedException(typeof (ArgumentNullException))]
        public void RequestCanNotNull()
        {
            _availabilityService.Get(null);
        }

        [Test]
        public void ValidRequestGetListsBack()
        {
            var availibiltyRequest = ObjectMother.GetValidJourneyRequestDto();
            var getAvailabilityCall = A.CallTo(() => _bookingServiceProxy.GetAvailability(A<AvailabilityRequest>.Ignored));
            var wsResponse = ObjectMother.GetValidAvailabilityResponse();
            getAvailabilityCall.Returns(wsResponse);

            JourneyAvailabilityDto response = _availabilityService.Get(availibiltyRequest);

            Assert.That(response.InboundJourneys.Count == wsResponse.InboundJourneys.Count() && response.OutboundJourneys.Count == wsResponse.OutoundJourneys.Count());
            getAvailabilityCall.MustHaveHappened();
        }

        [Test, ExpectedException(typeof (InvalidOperationException))]
        public void PastDepartureTimeIsInvalid()
        {
            var availibiltyRequest = ObjectMother.GetValidJourneyRequestDto();
            availibiltyRequest.DepartureDate = DateTime.Now.AddHours(-5);
            var getAvailabilityCall = A.CallTo(() => _bookingServiceProxy.GetAvailability(A<AvailabilityRequest>.Ignored));

            _availabilityService.Get(availibiltyRequest);

            getAvailabilityCall.MustNotHaveHappened();
        }
    }
}