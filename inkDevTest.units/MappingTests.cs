﻿using System;
using System.Linq;
using AutoMapper;
using InkDevTest.Service.BookingService;
using InkDevTest.Service.Dto;
using InkDevTest.Service.Mappings;
using NUnit.Framework;

namespace InkDevTest.units
{
    [TestFixture]
    public class MappingTests
    {
        [SetUp]
        public void Setup()
        {
            AutoMapping.CreateMaps();
        }

        [Test]
        public void JourneyRequestDtoToAvailabilityRequest()
        {
            var journeyRequestDto = ObjectMother.GetValidJourneyRequestDto();

            var mappedRequest = Mapper.Map<AvailabilityRequest>(journeyRequestDto);

            Assert.That(mappedRequest.DepartureDate == journeyRequestDto.DepartureDate &&
                        mappedRequest.Destination == journeyRequestDto.Destination &&
                        mappedRequest.Origin == journeyRequestDto.Origin &&
                        mappedRequest.ReturnDate == journeyRequestDto.ReturnDate);

        }

        [Test]
        public void JourneyToJourneyDto()
        {
            var journey = ObjectMother.GetValidJourney(DateTime.Now, DateTime.Now.AddHours(12));

            var mappedJourney = Mapper.Map<JourneyDto>(journey);

            Assert.That(mappedJourney.ArrivalStationName == journey.ArrivalStation.Name &&
                        mappedJourney.ArrivalTime == journey.ArrivalTime &&
                        mappedJourney.DepartureStationName == journey.DepartureStation.Name &&
                        mappedJourney.Number == journey.Number);
        }

        [Test]
        public void AvailabilityResponseToJourneyAvailabilityDto()
        {
            var availabilityResponse = ObjectMother.GetValidAvailabilityResponse();

            var mappedDto = Mapper.Map<JourneyAvailabilityDto>(availabilityResponse);

            Assert.That(mappedDto.InboundJourneys.Count == availabilityResponse.InboundJourneys.Count() &&
                        mappedDto.OutboundJourneys.Count == availabilityResponse.OutoundJourneys.Count());
        }
    }
}