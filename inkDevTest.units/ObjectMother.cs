﻿using System;
using System.Collections.Generic;
using InkDevTest.Service.BookingService;
using InkDevTest.Service.Dto;

namespace InkDevTest.units
{
    public static class ObjectMother
    {
         public static JourneyRequestDto GetValidJourneyRequestDto()
         {
             return new JourneyRequestDto
                 {
                     DepartureDate = DateTime.Now.AddHours(3),
                     Destination = "Chaing Mai",
                     Origin = "Camberwell",
                     ReturnDate = DateTime.Now.AddYears(2)
                 };
         }

        public static JourneyDto GetValidJourneyDto(DateTime departure, DateTime arrival)
        {
            return new JourneyDto{ ArrivalStationName = "Chaing Mai International", DepartureStationName = "Peckham Road Bus stop", DepartureTime = departure, ArrivalTime = arrival, Number = "1"};
        }

        public static Journey GetValidJourney(DateTime departure, DateTime arrival)
        {
            var arrivalStation = new Location {Code = "CHM", Name = "Chaing Mai International"};
            var departureStation = new Location {Code = "PKH", Name = "Peckham Road Bus stop"};

            return new Journey{ArrivalStation = arrivalStation, ArrivalTime = arrival, DepartureStation = departureStation, DepartureTime = departure, Number = "1"};
        }

        public static AvailabilityResponse GetValidAvailabilityResponse()
        {
            var response = new AvailabilityResponse();
            var outbound = new List<Journey>();
            var inbound = new List<Journey>();

            for (int i = 0; i <= 5; i++)
            {
                outbound.Add(GetValidJourney(DateTime.Now.AddHours(i), DateTime.Now.AddHours(i+12)));
                inbound.Add(GetValidJourney(DateTime.Now.AddYears(2).AddHours(i), DateTime.Now.AddYears(2).AddHours(i + 12)));
            }

            response.OutoundJourneys = outbound.ToArray();
            response.InboundJourneys = inbound.ToArray();
            return response;
        }


    }
}