﻿using System;
using Castle.Core;
using Castle.Windsor;
using InkDevTest.Service;
using InkDevTest.Service.DiConfiguration;
using InkDevTest.UI.Controllers;
using InkDevTest.UI.DiConfiguration;
using NUnit.Framework;

namespace InkDevTest.units
{
    [TestFixture]
    public class DiContainerTests
    {
        private IWindsorContainer _container;

        [SetUp]
        public void Setup()
        {
            _container = new WindsorContainer();
            ServiceRegistrations.AddTo(_container);
            ControllerRegistrations.AddTo(_container);
        }

        [TestCase(typeof(AvailabilityService))]
        [TestCase(typeof(BookingServiceProxy))]
        [TestCase(typeof(AvailabilityController))]
        public void CanResolveType(Type type)
        {
            Assert.That(_container.Kernel.HasComponent(type), "The type is not registered: " + type.Name);
        }

        [TestCase(typeof(AvailabilityService), LifestyleType.Transient)]
        [TestCase(typeof(BookingServiceProxy), LifestyleType.Transient)]
        [TestCase(typeof(AvailabilityController), LifestyleType.PerWebRequest)]
        public void CanResolveType(Type type, LifestyleType lifestyle)
        {
            var registeredLifestyle =
                _container.Kernel.ConfigurationStore.GetComponentConfiguration(type.FullName).Attributes["Lifestyle"];
            Assert.That(registeredLifestyle == lifestyle.ToString(), "The type does not have the right lifestyle: " + type.Name + " | " + lifestyle);
        }
    }
}