﻿using System;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using InkDevTest.Service.Dto;
using NUnit.Framework;
using Newtonsoft.Json;
using TechTalk.SpecFlow;

namespace InkDevTest.Spec.Steps
{
    [Binding]
    public class AvailabilityRequestSteps
    {
        private JourneyRequestDto _journeyRequestDto;
        private JourneyAvailabilityDto _journeyAvailabilityDto;

        [Given(@"I have a valid availability request")]
        public void GivenIHaveAValidAvailabilityRequest()
        {
            _journeyRequestDto = new JourneyRequestDto
                {
                    DepartureDate = DateTime.Now.AddHours(3),
                    ReturnDate = DateTime.Now.AddYears(2),
                    Destination = "Chaing Mai",
                    Origin = "Peckham"
                };
        }
        
        [Given(@"the journey is available")]
        public void GivenTheJourneyIsAvailable()
        {
            //Out of the control of these tests, in for completeness
        }
        
        [When(@"I submit this to the api")]
        public void WhenISubmitThisToTheApi()
        {
            var url = "http://localhost:50101/api/availability";
            var json = JsonConvert.SerializeObject(_journeyRequestDto);
            var client = new HttpClient();
            var result = client.PostAsync(url, new StringContent(json, Encoding.UTF8, "application/json"))
                               .ContinueWith(r => r.Result)
                               .Result.Content.ReadAsStringAsync().Result;
            _journeyAvailabilityDto = JsonConvert.DeserializeObject<JourneyAvailabilityDto>(result);
        }
        
        [Then(@"there should be results")]
        public void ThenTheResultShouldBeTrue()
        {
            Assert.That(_journeyAvailabilityDto.InboundJourneys.Count > 0 && _journeyAvailabilityDto.OutboundJourneys.Count > 0);
        }
    }
}