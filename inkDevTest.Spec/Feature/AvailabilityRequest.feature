﻿Feature: AvailabilityRequest
	In order to take a journey
	As a user
	I want to check the availability of a journey

@AvailabiltyRequest
Scenario: Check availity
	Given I have a valid availability request
    And the journey is available
	When I submit this to the api
	Then there should be results
