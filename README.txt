Please read the follwoing as it relates to the set up of the application:
_________________________________________________________________________

This is an MVC4 WebApi application that consumes the mock service, translates the proxy objects
into DTO and serves them to a HTML5 AngularJS front end.

The application is written using full TDD and BDD (this can be confirmed via commit history).  Unit
testing uses nUnit and BDD is done with SpecFlow, all managed via NuGet.  However if you don't have
the VS.Net extension for SpecFlow installed already then the .vsix file can be found in tools.

The app runs out of IIS Express port 50101.  If you wish to change this then you'll have to change both
the specs and angular controller to reflect this.

Asumptions:
__________________________________________________________________________

I took the liberty of writting a BDD specification for checking the availability.  No other methods in the service where consumed, only the 
availability check as specifified.

The app was build using the tracer bullet development methodolgy.


Many thanks
Jolyon Wharton  