﻿using AutoMapper;
using InkDevTest.Service.BookingService;
using InkDevTest.Service.Dto;

namespace InkDevTest.Service.Mappings
{
    public class AutoMapping
    {
        public static void CreateMaps()
        {
            Mapper.CreateMap<JourneyRequestDto, AvailabilityRequest>();
            Mapper.CreateMap<Journey, JourneyDto>();
            Mapper.CreateMap<AvailabilityResponse, JourneyAvailabilityDto>()
                  .ForMember(jad => jad.OutboundJourneys, opt => opt.MapFrom(ar => ar.OutoundJourneys));
        }
    }
}