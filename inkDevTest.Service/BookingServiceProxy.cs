﻿using InkDevTest.Service.BookingService;

namespace InkDevTest.Service
{
    public class BookingServiceProxy
    {
        private BookingClient _bookingClient;

        public virtual AvailabilityResponse GetAvailability(AvailabilityRequest request)
        {
            _bookingClient = new BookingClient();
            return _bookingClient.GetAvailability(request);
        }
    }
}