﻿using System;

namespace InkDevTest.Service.Dto
{
    public class JourneyDto
    {
        public string DepartureStationName { get; set; }
        public DateTime DepartureTime { get; set; }
        public string ArrivalStationName { get; set; }
        public DateTime ArrivalTime { get; set; }
        public string Number { get; set; }
    }
}