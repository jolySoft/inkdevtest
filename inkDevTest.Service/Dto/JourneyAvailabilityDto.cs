﻿using System.Collections.Generic;

namespace InkDevTest.Service.Dto
{
    public class JourneyAvailabilityDto
    {
        public List<JourneyDto> InboundJourneys { get; set; }
        public List<JourneyDto> OutboundJourneys { get; set; }
    }
}