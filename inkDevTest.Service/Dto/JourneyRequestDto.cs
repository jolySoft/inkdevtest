﻿using System;

namespace InkDevTest.Service.Dto
{
    public class JourneyRequestDto
    {
        public DateTime DepartureDate { get; set; }
        public DateTime ReturnDate { get; set; }
        public string Destination { get; set; }
        public string Origin { get; set; }
    }
}