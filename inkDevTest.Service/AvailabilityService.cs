﻿using System;
using AutoMapper;
using InkDevTest.Service.BookingService;
using InkDevTest.Service.Dto;

namespace InkDevTest.Service
{
    public class AvailabilityService
    {
        private readonly BookingServiceProxy _bookingServiceProxy;

        public AvailabilityService(BookingServiceProxy bookingServiceProxy)
        {
            _bookingServiceProxy = bookingServiceProxy;
        }


        public JourneyAvailabilityDto Get(JourneyRequestDto journeyRequest)
        {
            if(journeyRequest == null) throw new ArgumentNullException("journeyRequest");
            if(journeyRequest.DepartureDate < DateTime.Now) throw new InvalidOperationException("Can't start in the past, only in IT does this happen");
            var availabilityRequest = Mapper.Map<AvailabilityRequest>(journeyRequest);

            var availabilityResponse = _bookingServiceProxy.GetAvailability(availabilityRequest);

            return Mapper.Map<JourneyAvailabilityDto>(availabilityResponse);
        }
    }
}