﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;

namespace InkDevTest.Service.DiConfiguration
{
    public static class ServiceRegistrations
    {
        public static void AddTo(IWindsorContainer container)
        {
            container.Register(Component.For<AvailabilityService>().LifestyleTransient(),
                                Component.For<BookingServiceProxy>().LifestyleTransient());
        }
    }
}