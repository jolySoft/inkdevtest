﻿using System.Web.Http;
using InkDevTest.Service;
using InkDevTest.Service.Dto;

namespace InkDevTest.UI.Controllers
{
    public class AvailabilityController : ApiController
    {
        private readonly AvailabilityService _availabilityService;

        public AvailabilityController(AvailabilityService availabilityService)
        {
            _availabilityService = availabilityService;
        }
        
        public JourneyAvailabilityDto Post(JourneyRequestDto requestDto)
        {
            return _availabilityService.Get(requestDto);
        }
    }
}
