﻿using System.Web;
using System.Web.Http;
using System.Web.Routing;
using Castle.Windsor;
using InkDevTest.Service.DiConfiguration;
using InkDevTest.Service.Mappings;
using InkDevTest.UI.DiConfiguration;

namespace inkDevTest.UI
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class WebApiApplication : HttpApplication
    {
        private static IWindsorContainer _container;

        public IWindsorContainer Container
        {
             get { return _container; }
        }

        protected void Application_Start()
        {
            _container = new WindsorContainer();
            StartIoC();
            AutoMapping.CreateMaps();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        private void StartIoC()
        {
            ControllerRegistrations.AddTo(_container);
            ServiceRegistrations.AddTo(_container);
        }
    }
}