﻿using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Newtonsoft.Json;

namespace InkDevTest.UI.DiConfiguration
{
    public static class ControllerRegistrations
    {
        public static void AddTo(IWindsorContainer container)
        {
            container.Register(Classes.FromThisAssembly().BasedOn<IHttpController>().Configure(c => c.LifestylePerWebRequest()));
            GlobalConfiguration.Configuration.Services.Replace(typeof(IHttpControllerActivator), new WindsorHttpControllerActivator(container));
        }
    }
}